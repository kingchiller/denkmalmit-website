export default {
  jsonLd: {
    '@context': 'http://schema.org',
    '@type': 'Organization',
    address: {
      '@type': 'PostalAddress',
      addressLocality: 'Basel',
      addressRegion: 'BS',
      postalCode: '4058',
      streetAddress: 'Erlenstrasse 41'
    },
    name: 'Verein Denkmalmit',
    description: 'Denkmalmit - Der Verein hinter Denkmal.org',
    logo: 'https://denkmalmit.org/images/logo.svg',
    url: 'https://denkmalmit.org',
    email: 'kontakt@denkmal.org',
    slogan: 'Durch die Nacht mit Denkmal.org'
  }
}
