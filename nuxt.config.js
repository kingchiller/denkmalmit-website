import meta from './assets/meta'

export default {
  /*
   ** Headers of the page
   */
  head: {
    title: 'Denkmalmit - Der Verein hinter Denkmal.org',
    link: [
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Open+Sans:400,600,700'
      }
    ],
    __dangerouslyDisableSanitizers: ['script'], // for ld+json
    script: [
      {
        src:
          'https://cdn.jsdelivr.net/npm/basicscroll@2.0.0/dist/basicScroll.min.js'
      },
      {
        defer: true,
        src: 'https://use.fontawesome.com/releases/v5.0.8/js/all.js'
      },
      {
        innerHTML: JSON.stringify(meta.jsonLd),
        type: 'application/ld+json'
      }
    ]
  },

  meta: {
    description:
      'Denkmalmit ist der Verein hinter Denkmal.org, dem Online-Eventkalender für Basels Nachtleben.',
    theme_color: '#1a1a1a',
    ogHost: 'https://www.denkmalmit.org',
    ogImage: { path: '/images/denkmal-logo.png' },
    twitterCard: 'summary_large_image',
    twitterSite: '@denkmal_basel',
    twitterCreator: '@stophecom'
  },

  modules: ['@nuxtjs/sentry'],

  sentry: {
    public_key: process.env.SENTY_TOKEN || '3ed4d1fea97f4df4b5424aff9b05a2e9',
    project_id: process.env.SENTRY_PROJECT_ID || '1367711',
    environment: process.env.NODE_ENV
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: '#999999' },

  plugins: [
    {
      src: '~/plugins/vueSocialSharing.js'
    },
    {
      src: '~/plugins/vueScrollReveal',
      ssr: false
    }
  ],

  buildModules: [
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module',
    [
      '@nuxtjs/google-analytics',
      {
        id: 'UA-570541-7'
      }
    ]
  ]
}
